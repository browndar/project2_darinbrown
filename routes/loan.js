var express = require('express');
var router = express.Router();
var loan_dal = require('../model/loan_dal')


router.get('/all', function(req, res) {
    loan_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('loan/loanViewAll', { 'result':result });
        }
    });

});


module.exports = router;