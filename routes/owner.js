var express = require('express');
var router = express.Router();
var owner_dal = require('../model/owner_dal')


router.get('/all', function(req, res) {
    owner_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('owner/ownerViewAll', { 'result':result });
        }
    });

});





module.exports = router;