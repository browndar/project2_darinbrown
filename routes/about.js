var express = require('express');
var about_dal = require('../model/about_dal');
var router = express.Router();

router.get('/', function(req, res) {
    about_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('about/aboutViewAll', { 'result':result });
        }
    });

});

//router.get('/all',render('about/aboutViewAll'));



module.exports = router;